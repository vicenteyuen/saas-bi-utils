package test

/**
 * Unit test run on idea
 */

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/vicenteyuen/saas-bi-utils/ids"
	"testing"
)

/**
 * sample case 01 for generate id
 */
func TestID_case01(t *testing.T) {

	var idworker = ids.IdWorker{}

	nextId, _err := idworker.NextId()

	if _err == nil {

	}

	fmt.Print(nextId)

	assert.Equal(t, true, nextId > 1, "The value for is not expcet .")

}

/**
 * add test benchmark:
 *	go test -test.bench="Benchmark_ID_case01"
 */
func Benchmark_ID_case01(b *testing.B) {
	// --- stop first ----
	b.StopTimer()

	b.StartTimer()

	var idworker = ids.IdWorker{}

	for i := 0; i < b.N; i++ {

		_, _err := idworker.GenerateId()

		if _err == nil {

		}

	}
}
