package test

import (
	"testing"
	"github.com/gin-gonic/gin"
	"net/http"
	"go.uber.org/fx"
	"gitlab.com/vicenteyuen/saas-bi-utils/routerengine"
	"fmt"
)

/**
 * sample case 01 for generate id
 */
func TestGinRouter_case01(t *testing.T) {
	
	routerEngine := routerengine.RouterManager{}.CreateRouterEngine()
	
	routerEngine.PreStart()
	
	routerEngine.Start()
	
	routerEngine.PostStart()
	
	fmt.Print( routerEngine )

	
	/*

	router := gin.Default()


	var manager routermanager.GinRouterManager = routermanager.GinRouterManager{router,8080}


	manager.BindHandler(routermanager.METHOD_GET , "/" , Handle_get)
	
	
	manager.Start()
	*/

	/*
	router.GET("/", func(c *gin.Context) {
		c.String(http.StatusOK, "Hello World")
	})
	*/

	//router.Run(":8000")

}


func Handle_get(c *gin.Context) {
	c.String(http.StatusOK, "Hello World")
}