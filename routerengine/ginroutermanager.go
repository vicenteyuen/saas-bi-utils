package routerengine

import "github.com/gin-gonic/gin"

/**
 *
 */
type RouterManager struct {
}



// Bind Http Router Manager
func (manager RouterManager) CreateRouterEngine() (routerEngine *RouterEngine) {

	engine := gin.Default()

	routerEngine = &RouterEngine{engine}
	
	return routerEngine
}

