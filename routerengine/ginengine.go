package routerengine

import (
	"github.com/gin-gonic/gin"
)


var METHOD_GET int8 = 1
var METHOD_POST int8 = 2
var METHOD_DELETE int8 = 4
var METHOD_PUT int8 = 3



/**
 *
 */
type RouterEngine struct {

	engine *gin.Engine

}

// --- call bind handler ----
func (engine RouterEngine) BindHandler(method int8 , relativePath string, handler gin.HandlerFunc) {


}

/**
 * public start interface 
 */
func (engine RouterEngine) PreStart() {
	
}

/**
 * public post start interface 
 */
func (engine RouterEngine) PostStart() {
	
}

// --- start router mapping ---
func (engine RouterEngine) Start() {

}

/**
 * stop router engine 
 */
func (engine RouterEngine) Stop() {
	
}